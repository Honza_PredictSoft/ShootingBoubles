Simple 2D side scrolling arcade game. Fixed world made by bunch of blocks. 2 local players on the same screen. At the beginning each player has a default weapon (pistol) and 100 HP. Bunch of bonus boxes randomly appears at the specific locations. Goal is to kill the other one.

Only one weapon can be caried / multipple weapons & switching them
Limited / unlimited ammo

Bonuses
 - Medicine
	- add 30 HP
	- no HP limit
 - Increased damage (+20%)
	- works for all weapons type
 - Increased fire spead (+20%)
	- works for all weapons type
 - Pistol upgrade
	+5 dmg
	+10% fire speed
 - Machine gun (upgrade)
	+2 dmg
	+10% fire speed
 - Shot gun (upgrade)
	+3 dmg
	+10% fire speed
 - Mine (upgrade)
	+20 dmg
	+10% fire speed
	+10% damage area
 - Granate (upgrade)
	+10 dmg
	+10% fire speed
	+10% damage area
 - Bazooka (upgrade)
	+10 dmg
	+10% fire speed
	+10% damage area

Weapons
 - Pistol
	- 10 dmg
	- 1 bullet / 1s
- Machine gun
	- 5 dmg
	- 5 bullet / 1s
- Shot gun
	- 5 bullets
	- 5 dmg
	- 1 shot / 1s
- Bazooka
	- area demage
	- 25 dmg
	- 1 shoot / 2s
- Granade
	- ellipse trajectory
	- area demage
	- explodes on contact
	- 30 dmg
	- 1 granate / 3s
- Mine
	- land mine
	- explode on contact with any player
	- area dmg
	- 50 dmg
	- 1 mine / 5s